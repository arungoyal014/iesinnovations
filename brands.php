<?php include 'header.php'; ?>

<div class="banner-area bg-overlay" id="banner-area" style="background-image:url(images/banner/about_banner.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1 class="banner-title">Our <span>Brands</span></h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Our Brands</li>
                    </ol><!-- Breadcumb End -->
                </div><!-- Banner Heading end -->
            </div><!-- Col end-->
        </div><!-- Row end-->
    </div><!-- Container end-->
</div><!-- Banner area end-->

<section id="main-container" class="main-container ts-srevice-inner pb-120">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">
                    <span>Our</span> Brands
                </h2>
            </div><!-- Col end -->
        </div><!-- Row End -->

        <div class="row">
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/katashi-pneumatic-tools-2021.pdf" target="_blank">
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid" src="images/logo1.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/panasonic_power_tool_catalogu_asia.pdf" target="_blank">
                    <div class="ts-service-wrapper brand">				
                        <span class="service-img">
                            <img class="img-fluid" src="images/logo2.png" alt="">
                        </span> <!-- Service Img end -->                
                    </div> <!-- Service wrapper end -->
                </a>	
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo3.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <a href="javascript:void()">
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid" src="images/logo4.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end -->            
        </div><!-- Row end -->

        <div class="gap-30"></div>
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <a href="#" data-toggle="modal" data-target="#myModal" >
                    <div class="ts-service-wrapper brand new">
                        <span class="service-img">
                            <img class="img-fluid" src="images/logo5.png" alt="">
                        </span> <!-- Service Img end -->                
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/Kanon_catalog_ new.pdf" target="_blank">
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid m-34" src="images/kenon-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo6.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo7.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 

        </div><!-- Row end -->

        <div class="gap-30"></div>
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">                    
                        <span class="service-img">
                            <img class="img-fluid m-34" src="images/baker-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->                
            </div> <!-- Col end -->            
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">                    
                    <span class="service-img">
                        <img class="img-fluid m-34" src="images/luthra-logo.png" alt="">
                    </span> <!-- Service Img end -->
                </div> <!-- Service wrapper end -->                
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo11.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo12.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
        </div><!-- Row end -->

        <div class="gap-30"></div>
        <div class="row">             
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/Molygraph_General_Engineering_Brochure -2018.pdf" target="_blank"  >
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid m-34" src="images/molygraph-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/logo9.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                        
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/Lubricants-Pidilite.pdf" target="_blank"  >
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid" src="images/pidilite-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <a href="#" data-toggle="modal" data-target="#myModal5">
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid" src="images/herutu-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end -->
        </div><!-- Row end -->
        
        <div class="gap-30"></div>
        <div class="row">             
            <div class="col-lg-3 col-md-12">
                <a href="pdfs/makita-product-catalogue-2021.pdf" target="_blank"  >
                    <div class="ts-service-wrapper brand">
                        <span class="service-img">
                            <img class="img-fluid m-34" src="images/makita-logo.png" alt="">
                        </span> <!-- Service Img end -->                 
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
        </div><!-- Row end -->

    </div><!-- Container end -->
</section><!-- Main container end -->  



<?php include 'footer.php'; ?> 