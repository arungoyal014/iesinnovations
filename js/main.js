
$(function ($) {
   "use strict";


   $(window).on('load', function () {
      /* -----------------------------------------
                  prelaoder
      -------------------------------------------- */
      $('#preloader').addClass('loaded');

      $('.prelaoder-btn').on('click', function (e) {
         e.preventDefault();
         if (!($('#preloader').hasClass('loaded'))) {
            $('#preloader').addClass('loaded');
         }
      })
   })

   /* ---------------------------------------------
                     Menu Toggle 
   ------------------------------------------------ */

   if ($(window).width() < 991) {
      $(".navbar-nav li a").on("click", function () {
         $(this).parent("li").find(".dropdown-menu").slideToggle();
         $(this).find("i").toggleClass("fa-angle-up fa-angle-down");
      });
   }

   /* ---------------------------------------------
                     Main Slider 
   ------------------------------------------------ */

   $(".ts-slider-area").owlCarousel({
      items: 1,
      loop: true,
      smartSpeed: 900,
      dots: false,
      animateOut: 'fadeOut',
      autoplay:true,
      autoplayTimeout:4000,
      nav: false,
//      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],      
      mouseDrag: true,
      responsive: {
         0: {
            nav: false,
            mouseDrag: false,
         },
         600: {
            nav: false,
            mouseDrag: false,
         },
         1000: {
            nav: true,
            mouseDrag: true,
         }
      }
   });

   /* ---------------------------------------------
                     Intro Slider 
   ------------------------------------------------ */

   $(".intro-content-carousel").owlCarousel({
      items: 1,
      loop: true,
      smartSpeed: 900,
      dots: false,
      nav: true,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      autoplay: false
   });

   /* ---------------------------------------------
                     Service Slider 
   ------------------------------------------------ */

   $(".service-carousel").owlCarousel({
      items: 3,
      loop: true,
      smartSpeed: 900,
      dots: false,
      nav: true,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      autoplay: false,
	  responsiveClass:true,	  
      responsive: {
         0: {
            items: 1,
         },
         600: {
            items: 2,
         },
         1000: {
            items: 3,
         }
      }
   });

   /* ---------------------------------------------
                     Partner Slider 
   ------------------------------------------------ */

   $(".partner-carousel").owlCarousel({
      items: 5,
      loop: true,
      smartSpeed: 900,
      dots: false,
      nav: false,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      autoplay: true,
      responsive: {
         0: {
            items: 2,
         },
         600: {
            items: 3,
         },
         1000: {
            items: 5,
         }
      }
   });

   /* ---------------------------------------------
                     Site Search 
   ------------------------------------------------ */

   $(".search_btn").on("click", function () {
      $(this).toggleClass("show");
      $(".search-box").toggleClass("show");      
   });
   
   
   /* ---------------------------------------------
                     Product detail leftmenu
   ------------------------------------------------ */
   
   $(".left_panel").on("click", function () {
	  $("ul.service-menu").slideToggle(300); 	  
   });
  
   /* ---------------------------------------------
                     Back to top 
   ------------------------------------------------ */
   $('#back-to-top').on('click', function () {
      $('#back-to-top').tooltip('hide');
      $('body,html').animate({
         scrollTop: 0
      }, 800);
      return false;
   });

   $('#back-to-top').tooltip('hide');

   /* ---------------------------------------------
                        Progress Bar 
      ------------------------------------------------ */
   function isScrolledIntoView(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
   }

   $(window).scroll(function () {
      if ($('.progress-bar').lentgh > 0) {
         if (isScrolledIntoView('.progress-bar') === true) {
            $('.progress-bar').css(
               'transition-duration', '2s'
            );
         }
      }

   });

   if ($('.progress .progress-bar').length > 0) {
      $('.progress .progress-bar').css("width", function () {
         return $(this).attr("aria-valuenow") + "%";
      });
   }
});

