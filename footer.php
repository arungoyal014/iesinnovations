<div class="download-catlog">
    <a href="pdfs/ies-catalog.pdf" target="blank">
        <img src="images/download-th.jpg" alt="">
    </a>
</div>
<!-- Modal -->
<div class="modal fade custom-modal" id="myModal2" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Surface Plates</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <a href="pdfs/Luthra_new _1.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Luthra New Catalogue</h4>
                            <p>Surface Plates</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/Luthra_new_2.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Luthra Catalogue</h4>
                            <p>Surface Plates</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/luthra-catalogue-2.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Luthra Catalogue 2</h4>
                            <p>Granite Comparator Stands</p>
                        </div>
                    </div>
                </a>

            </div> 		

        </div>      
    </div>
</div> 

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal3" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>TORQUE WRENCHES</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <a href="pdfs/Kanon_catalog_ new.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Kanon Catalog</h4>
                            <p>Torque Wreches</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/Tohnichi-Catalog.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Tohnichi Catalog</h4>
                            <p>Torque Wreches</p>
                        </div>
                    </div>
                </a>

            </div> 		

        </div>      
    </div>
</div> 

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal4" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Gauges</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <img src="images/gauges.jpg" alt="" class="img-fluid">
            </div>
        </div>      
    </div>
</div>

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal5" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>ERROR PROOFING SYSTEMS</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <a href="pdfs/pokayoke_tools_general_catalog_eng_2019 (1).pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pokayoke Tools TW-800 Series</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_general_catalog_separate_volume.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pokayoke Tools TW-800 Series Separate Volume</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_tw_850t_catalog_eng_2020.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pokayoke Compact Transmitter TW-850T</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_hcp-2402t-mc_leaflet_eng_2019.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pokayoke Wireless Unit HCP-2402T-MC for Impact Driver/Impact Wrench(Makita Corporation)</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_pokayoke_plus_catalog_eng_2020.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Production Process Support Software for Pokayoke Tools POKAYOKE plus</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_tw_800r_scl_catalog_eng_2018.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Simple Pokayoke Counter TW-800R-SCL</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/pokayoke_tools_general_catalog_eng_2019.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Simple Pokayoke Counter TW-800R-SC</h4>
                            <p>Pokayoke Tools</p>
                        </div>
                    </div>
                </a>
               
            </div> 		

        </div>      
    </div>
</div> 

<div class="modal fade custom-modal" id="myModal6" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>MEASURING INSTRUMENTS</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <a href="pdfs/insize-catalog.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Insize Catalog</h4>
                            <p>Measuring Instruments</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/mitutoyo-catalog.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Mitutoyo Catalog</h4>
                            <p>Measuring Instruments</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/baker-catalog.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Baker Catalog</h4>
                            <p>Measuring Instruments</p>
                        </div>
                    </div>
                </a>
                <a href="javascript:void();">
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Kanon Catalog</h4>
                            <p>Measuring Instruments</p>
                        </div>
                    </div>
                </a>

            </div> 		

        </div>      
    </div>
</div> 

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal7" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Gauges</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <img src="images/pidilite-description.jpg" alt="" class="img-fluid">
            </div>
        </div>      
    </div>
</div>

<div class="modal fade custom-modal" id="myModal8" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Dial Stands</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png"></button>         
            </div>
            <div class="modal-body"> 
                <a href="pdfs/1149_down_file.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Deburring System - Full PDF Catalog</h4>
                            <p>Dial Stands</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/1150_down_file.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Holding System - Full PDF Catalog</h4>
                            <p>Dial Stands</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/1152_110823_down_file.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Noga General Catalog - Full PDF File</h4>
                            <p>Dial Stands</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/1171_167934_down_file.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">New Products 2020</h4>
                            <p>Dial Stands</p>
                        </div>
                    </div>
                </a>

            </div> 		

        </div>      
    </div>
</div> 

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal9" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Miscellaneous Solutions</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png" alt=""></button>         
            </div>
            <div class="modal-body"> 
                <img src="images/miscellaneous.jpg" alt="" class="img-fluid">
            </div>
        </div>      
    </div>
</div>

<!-- Modal -->
<div class="modal fade custom-modal" id="myModal" role="dialog">
    <div class="modal-dialog">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Noga Catalogue</h2>
                <button type="button" class="close" data-dismiss="modal"><img src="images/close.png"></button>             
            </div>
            <div class="modal-body"> 

                <a href="pdfs/NOGA_Deburring.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Noga Deburring Catalog</h4>
                            <p>Deburring System</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/Noga_Holding.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Noga Holding Catalog</h4>
                            <p>Holding System</p>
                        </div>
                    </div>
                </a>
                <a href="pdfs/NOGA_MINICOOL.pdf" target="_blank" >
                    <div class="media">
                        <div class="media-left">
                            <img src="images/pdf-icon.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Noga Minicool Catalog</h4>
                            <p>Cooling System</p>
                        </div>
                    </div>
                </a>

            </div>        
        </div>

    </div>
</div>  

<footer class="footer" id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 footer-box">
                    <i class="icon icon-map-marker2"></i>
                    <div class="footer-box-content">
                        <h3>IES Innovations</h3>
                        <p>Udham Singh Nagar, 263153</p>
                    </div>
                </div><!-- Box 1 end-->
                <div class="col-md-4 footer-box">
                    <i class="icon icon-phone3"></i>
                    <div class="footer-box-content">
                        <h3>
                            <a href="tel:+91-78959-63836">+91-78959-63836</a> <br/>
                            <a href="tel:+91-97612-13836">+91-97612-13836</a>
                        </h3>
                        <p>Give us a call</p>
                    </div>
                </div><!-- Box 2 end-->
                <div class="col-md-4 footer-box">
                    <i class="icon icon-envelope"></i>
                    <div class="footer-box-content">
                        <h3>
                            <a href="mailto:dhananjay@iesinnovations.com">dhananjay@iesinnovations.com</a> <br/>
                            <a href="mailto:jagjeet@iesinnovations.com">jagjeet@iesinnovations.com</a>
                        </h3>
                        <p>24/7 online support</p>
                    </div>
                </div><!-- Box 3 end-->
            </div><!-- Content row end-->
        </div><!-- Container end-->
    </div><!-- Footer top end-->
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-widget footer-about">
                    <div class="footer-logo">
                        <a href="index-2.php">
                            <img class="img-fluid" src="images/footer_logo.png" alt="">
                        </a>
                    </div>
                    <p>IES Innovations is a leading manufacturing, importing, exporting and trading company of industrial products. With over 25 years of  actively working in the industrial sector, we give solutions to every major manufacturing company.</p>

                </div> <!-- Col End -->
                <!-- About us end-->
                <div class="col-lg-3 col-md-6 footer-widget widget-service">
                    <h3 class="widget-title"><span>Our</span> Products</h3>
                    <div class="outer_unstyled">
                        <ul class="unstyled">
                            <li><a href="product.php">Pneumatic Tools</a></li>
                            <li><a href="product.php">Battery Tools</a></li>
                            <li><a href="product.php">Torque Wrenches</a></li>
                            <li><a href="product.php">Gauges</a></li> 
                            <li><a href="product.php">Coils</a></li>                   
                        </ul> <!-- Ul end -->
                    </div>
                </div> <!-- Col End -->
                <div class="col-lg-3 col-md-6 footer-widget news-widget">
                    <h3 class="widget-title"><span>Quick</span> Links</h3>
                    <div class="outer_unstyled">
                        <ul class="unstyled">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php"> About us</a></li>
                            <li><a href="product.php"> Our Solutions</a></li>
                            <li><a href="brands.php"> Our Brands</a></li>
                            <li><a href="contact.php"> Contact Us</a></li>                     
                        </ul> <!-- Ul -->
                    </div>
                </div> <!-- Col End -->
                <div class="col-lg-3 col-md-6 footer-widget footer-about footer-address">
                    <h3 class="widget-title"><span>Contact</span> Us</h3>
                    <div class="outer_unstyled">
                        <p class="location">Plot No.6, Rudra Greens, Jainagar No.4, Near HP Petrol Pump, Dineshpur Road, Rudrapur, Distt. Udham Singh Nagar-263153, Uttarakhand</p>                        
                        <p class="mobile"><a href="tel:+91-78959-63836">+91-78959-63836</a>, <a href="tel:+91-97612-13836">+91-97612-13836</a>, <br><a href="tel:+91-84779-83000">+91-84779-83000</a></p>
                        <p class="email"><a href="mailto:dhananjay@iesinnovations.com">dhananjay@iesinnovations.com</a>, <a href="mailto:jagjeet@iesinnovations.com">jagjeet@iesinnovations.com</a></p>
                    </div>                  
                </div> <!-- Col End -->
            </div><!-- Content row end-->
        </div><!-- Container end-->
    </div><!-- Footer Main-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="copyright-info"><span>Copyright © <?php echo date('Y'); ?> IES Innovations.</span></div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="copyright-info text-right">
                        <span>Developed &amp; Managed By <a href="https://zapbase.com" target="blank"><img src="images/logo-white.png" alt=""></a></span>
                    </div>
                </div> <!-- Col End -->
            </div><!-- Row end-->
        </div><!-- Container end-->        
    </div><!-- Copyright end-->
</footer> <!-- Footer End -->


<!-- initialize jQuery Library -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.treeview.js"></script>
<!-- Popper JS -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap jQuery -->
<script src="js/bootstrap.min.js"></script>
<!-- Owl Carousel -->
<script src="js/owl-carousel.2.3.0.min.js"></script>
<!-- START js copy section -->
<script src="js/validate.js"></script>
<script src="js/underscore-min.js"></script>


<script src="js/main.js"></script>
<script>
    var winWidth = $(window).width();
    if (winWidth <= 767) {
        $(".widget-title").click(function () {
            $(this).parent().siblings().children('.outer_unstyled').slideUp(300);
            $(this).next().slideToggle(300);
            $(this).parent().siblings().children('.widget-title').removeClass('accordian-open');
            $(this).toggleClass('accordian-open');
        });
    }
    $(document).ready(function () {
        $("#browser").treeview({
            collapsed: true
        });

        $('.btnmenu').click(function () {
            $('#mobilemenu, .mobileback').fadeIn(700);
            $('#mobilemenu').stop(true, true).animate({'left': '0%'});
        });
        $('.mobileback').click(function () {
            $('.mobileback').fadeOut(500);
            $('#mobilemenu').stop(true, true).animate({'left': '-100%'});
            $('.search-box').removeClass("show");
            $('.icon-cross').removeClass("show");
            $('.icon-search').addClass("show");
        });
        $(".minimobilebut").click(function () {
            $(".prctupperlinks, .projectlinks").slideToggle("slow");
        });
        $(".mobilemenuclose").click(function () {
            $(" .mobileback").fadeOut(500);
            $("#mobilemenu").animate({'left': '-100%'});
        });
    });
</script>
</body>
