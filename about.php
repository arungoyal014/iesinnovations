<?php include 'header.php'; ?>

<div class="banner-area bg-overlay" id="banner-area" style="background-image:url(images/banner/about_banner.jpg);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h1 class="banner-title">About <span>Us</span></h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>About Us</li>
                    </ol>
                </div> <!-- Banner heading -->
            </div><!-- Col end-->
        </div><!-- Row end-->
    </div><!-- Container end-->
</div><!-- Banner area end-->

<section id="main-container" class="main-container pb-0">
    <div class="ts-about-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">
                        <span>About</span> IES Innovations
                    </h2>
                </div><!-- Col end -->
            </div><!-- Row End -->
            <div class="row overflow-hidden no-gutters">
                <div class="col-lg-7 col-md-12">
                    <div class="box-skew-hidden-left">
                        <div class="box-skew-left">
                            <img class="img-fluid" src="images/about/about-img2.jpg" alt="">
                        </div><!-- Box skew left -->
                    </div>
                </div> <!-- Col End -->
                <div class="col-lg-5 col-md-12">
                    <div class="box-skew-right">
                        <div class="box-content-wrapper">
                            <i class="icon-repair"></i>
                            <h2 class="column-title no-border">
                                <span>25 Years of</span> Experience 
                            </h2>
                            <p>IES Innovations is a leading manufacturing, importing, exporting and trading company of industrial products. With over 25 years of  actively working in the industrial sector, we give solutions to every major manufacturing company. This has helped us develop expertise in various sectors of manufacturing process. We are focused on constantly improving manufacturing solutions to enhance productivity for industrial sectors while maintaining cost to performance ratio.</p>
                        </div> <!-- Content wrapper end -->
                    </div> <!-- Content Right End -->
                </div> <!-- Col end -->
            </div> <!-- Row End -->
        </div> <!-- Container Fluid -->
    </div><!-- Ts About Us end -->
</section> <!-- About End -->

<section id="ts-history-tab" class="ts-history-tab">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 box-content-wrapper custom-heading">
<!--                        <i class="icon-history"></i>
                        <h2 class="column-title no-border">
                            <span>Our</span> Vision
                        </h2>-->                        
                    </div><!-- Col end -->
                    <div class="col-lg-12 col-md-12">
                        <div class="tab-content ts-tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <ul class="check-list unstyled">
                                    <li>IES Innovations is focused on constantly improving manufacturing solutions to enhance productivity for industrial sectors while maintaining cost to performance ratio.</li>
                                    <li>The product portfolio is manager to connect various segments of the manufacturing processes. The helps us create innovative complete solution for companies looking for a one stop shop.</li>
                                    <li>We have developed a professional network of sales and service executives to provide innovative cost -effective solutions across all major industrial clusters within india.</li>
                                    
                                </ul>
                                <p>&nbsp;</p>
                                <h2 class="column-title no-border">
                                    <span>Our</span> Vision
                                </h2>
                                <p>To be recognized for providing constant improvisation and solution to increses efficiency and productivity to manufacturing companies.</p>
                            </div> <!-- tab pane end -->                           
                        </div> <!-- tab content -->
                    </div> <!-- col end -->
                </div><!-- Row end -->
            </div> <!-- Col end -->
            <div class="col-lg-5 col-md-12 text-right">
                <span><img class="img-fluid" src="images/our-history.png" alt=""></span>
            </div> <!-- COl end -->
        </div> <!-- Row End -->        
    </div> <!-- Container end -->
</section> <!-- History tab end -->

  



<?php include 'footer.php'; ?> 