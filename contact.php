<?php include 'header.php'; ?>

<div class="banner-area bg-overlay" id="banner-area" style="background-image:url(images/banner/contact_banner.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1 class="banner-title">Contact <span>Us</span></h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Contact Us</li>
                    </ol>
                    <!-- Breadcumb End -->
                </div>
                <!-- Banner Heading end -->
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
<!-- Banner area end-->

<section id="main-container" class="main-container ts-contact-us">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-8 col-md-12">
                <div class="mapouter">
                    <div class="gmap_canvas">                  
                        <iframe height="555" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4269.9790900223625!2d79.38356548776189!3d29.034971823424524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a08132190de145%3A0x6febb9b9dc0691d5!2s4%2C%20Dineshpur%20Rd%2C%20Jainagar%2C%20Uttarakhand%20263153!5e0!3m2!1sen!2sin!4v1607420354395!5m2!1sen!2sin"></iframe>
                    </div>
                </div>
                <!-- Map End -->
            </div>
            <!-- Col end -->
            <div class="col-lg-4 col-md-12">
                <div class="contact-details">
                    <h2 class="column-title no-border text-white">
                        <span>Contact</span> Details
                    </h2>
                    <div class="contact-info-item">
                        <h3 class="column-title no-border text-white">
                            <span>Find</span> Location
                        </h3>
                        <p>Plot No.6, Rudra Greens, Jainagar No.4, Near HP Petrol Pump, Dineshpur Road, Rudrapur, Distt. Udham Singh Nagar-263153, Uttarakhand</p>
                    </div>                    

                    <div class="contact-info-item">
                        <h3 class="column-title no-border text-white">
                            <span>Mobile</span> No.
                        </h3>
                        <p><a href="tel:+91-78959-63836">+91-78959-63836</a>, 
                            <a href="tel:+91-97612-13836">+91-97612-13836</a> <br>
                            <a href="tel:+91-84779-83000">+91-84779-83000</a>
                        </p>
                    </div>
                    <div class="contact-info-item">
                        <h3 class="column-title no-border text-white">
                            <span>Mail</span> Us
                        </h3>
                        <p>
                            <a href="mailto:dhananjay@iesinnovations.com">dhananjay@iesinnovations.com</a>, 
                            <a href="mailto:jagjeet@iesinnovations.com">jagjeet@iesinnovations.com</a>
                        </p>
                        <br/>
                    </div>
                </div>
                <!-- Contact details end -->
            </div>
            <!-- Col End -->
        </div>
        <!-- row end -->
        <div class="gap-75"></div>
        <div class="row">

            <div class="col-lg-8">
                <h2 class="section-title">
                    <span>Ask</span> A Question
                </h2>
                <div id="contact-form" class="form-container contact-us">
                    <!-- START copy section: Hotel Contact Form -->
                    <div id="sendmessage" style="display:none;">Your message has been sent. Thank you!</div>
                    <form class="contactMe" id="myform">
                        <section>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <input type="text" name="name" class="field" placeholder="Name">
                                    <div class="col-sm-6 messages"></div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="tel" name="phone"  class="field" placeholder="Phone">
                                    <div class="col-sm-6 messages"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="email" name="email" class="field" placeholder="Email">
                                    <div class="col-sm-6 messages"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <textarea name="message" class="field" placeholder="Message"></textarea>
                                    <div class="col-sm-6 messages"></div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Send Message</button>
                        </section>
                        <!-- Ection end -->
                    </form>
                    <!-- END copy section:Service Contact Form -->
                </div>
                <!-- Contact form end -->
            </div>
            <div class="col-lg-4">
                <div class="sidebar sidebar-right">

                    <div class="widget">
                        <h3 class="widget-title"><span>Working</span> Hours</h3>
                        <ul class="unstyled service-time">
                            <li>
                                <span>Monday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                            <li>
                                <span>Tuesday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                            <li>
                                <span>Wednesday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                            <li>
                                <span>Thursday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                            <li>
                                <span>Friday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                            <li>
                                <span>Saturday</span>
                                <span>9:30 - 6:30</span>
                            </li>
                        </ul>
                    </div>
                    <!-- Working time end -->

                </div>


            </div> <!-- Col end -->
        </div> <!-- Row end -->


    </div>
</div>

</div>
<!-- Container end -->
</section>
<!-- Main container end -->

<script>
    (function () {
        // These are the constraints used to validate the form
        var constraints = {
            email: {
                // Email is required
                presence: true,
                // and must be an email (duh)
                email: true
            },

            name: {
                // You need to pick a username too
                presence: true,
                // And it must be between 3 and 20 characters long
                length: {
                    minimum: 3,
                    maximum: 20
                },
                format: {
                    pattern: "[a-zA-Z ]+",
                    // but we don't care if the username is uppercase or lowercase
                    flags: "i",
                    message: "can only contain a-z"
                }
            },

            phone: {
                presence: true,
                numericality: {
                    onlyInteger: true,
                }
            },

            message: {
                presence: true,
            },
        };

        // Hook up the form so we can prevent it from being posted
        var form = document.querySelector("form#myform");
        form.addEventListener("submit", function (ev) {
            ev.preventDefault();
            handleFormSubmit(form);
        });

        // Hook up the inputs to validate on the fly
        var inputs = document.querySelectorAll("input, textarea");
        for (var i = 0; i < inputs.length; ++i) {
            inputs.item(i).addEventListener("change", function (ev) {
                var errors = validate(form, constraints) || {};
                showErrorsForInput(this, errors[this.name])
            });
        }

        function handleFormSubmit(form, input) {
            // validate the form aainst the constraints
            var errors = validate(form, constraints);
            // then we update the form to reflect the results
            showErrors(form, errors || {});
            if (!errors) {
                showSuccess();
            }
        }

        // Updates the inputs with the validation errors
        function showErrors(form, errors) {
            // We loop through all the inputs and show the errors for that input
            _.each(form.querySelectorAll("input[name],textarea[name]"), function (input) {
                // Since the errors can be null if no errors were found we need to handle
                // that
                showErrorsForInput(input, errors && errors[input.name]);
            });
        }

        // Shows the errors for a specific input
        function showErrorsForInput(input, errors) {
            // This is the root of the input
            var formGroup = closestParent(input.parentNode, "form-group")
                    // Find where the error messages will be insert into
                    , messages = formGroup.querySelector(".messages");
            // First we remove any old messages and resets the classes
            resetFormGroup(formGroup);
            // If we have errors
            if (errors) {
                // we first mark the group has having errors
                formGroup.classList.add("has-error");
                // then we append all the errors
                _.each(errors, function (error) {
                    addError(messages, error);
                });
            } else {
                // otherwise we simply mark it as success
                formGroup.classList.add("has-success");
            }
        }

        // Recusively finds the closest parent that has the specified class
        function closestParent(child, className) {
            if (!child || child == document) {
                return null;
            }
            if (child.classList.contains(className)) {
                return child;
            } else {
                return closestParent(child.parentNode, className);
            }
        }

        function resetFormGroup(formGroup) {
            // Remove the success and error classes
            formGroup.classList.remove("has-error");
            formGroup.classList.remove("has-success");
            // and remove any old messages
            _.each(formGroup.querySelectorAll(".help-block.error"), function (el) {
                el.parentNode.removeChild(el);
            });
        }

        // Adds the specified error with the following markup
        // <p class="help-block error">[message]</p>
        function addError(messages, error) {
            var block = document.createElement("p");
            block.classList.add("help-block");
            block.classList.add("error");
            block.innerText = error;
            messages.appendChild(block);
        }

        function showSuccess() {
            // We made it \:D/
            $.ajax({
                url: 'ajax',
                data: {
                    data: $('#myform').serialize(),
                },
                type: 'POST',
                async: true,
                success: function (html) {
                    $('#sendmessage').show();
                    form.reset();
                }
            });
        }
    })();
</script>

<?php include 'footer.php'; ?> 