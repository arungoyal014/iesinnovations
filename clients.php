<?php include 'header.php'; ?>

<div class="banner-area bg-overlay" id="banner-area" style="background-image:url(images/banner/about_banner.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1 class="banner-title">Our <span>Clients</span></h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Our Clients</li>
                    </ol><!-- Breadcumb End -->
                </div><!-- Banner Heading end -->
            </div><!-- Col end-->
        </div><!-- Row end-->
    </div><!-- Container end-->
</div><!-- Banner area end-->

<section id="main-container" class="main-container ts-srevice-inner pb-120">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">
                    <span>Our</span> Clients
                </h2>
            </div><!-- Col end -->
        </div><!-- Row End -->



        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/maruti-suzuki.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/honda-motorcycle.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/honda-car.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/ashok-leyland.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->            
        </div><!-- Row end -->
        <div class="gap-30"></div>

        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/hero-motocorp.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/tata-motors.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/rico-group.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/jtkt.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->            
        </div><!-- Row end -->
        <div class="gap-30"></div>



        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/asahi-india.jpg" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/Keihin.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/bajaj-motor.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/mahle-filter.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->            
        </div><!-- Row end -->
        <div class="gap-30"></div>


        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/cnh_industrial.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/hitachi-chemical.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/powerspack_logo.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/client-gabriel.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->            
        </div><!-- Row end -->
        <div class="gap-30"></div>


        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/munjal.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/minda-group.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/continental-engine.png" alt="">
                    </span> <!-- Service Img end -->                 
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/jcb-logo.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->            
        </div><!-- Row end -->
        <div class="gap-30"></div>


        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/endurance-logo.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/ufi-filter.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/amtek.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/bharat-benz.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/bharat-sg.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/bkt.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/bry-air.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/chaparo.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/eicher-tracktor.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/fcc.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/force-motors.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/gabriel.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/gmax.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/harley-davidson.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/havells.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/hero-cycle.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/indo-aotomatic.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/jbm-group.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/jtekt.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/kangaro.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/magnet-mareli.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/munjal-kiriu.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/panasonic.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/pricol.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/rico.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/rockman.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/skh-metal.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/sonalika.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>        
        
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/spm.png" alt="">
                    </span> <!-- Service Img end -->               
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end --> 
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/swaraj.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/tata-hitachi.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
            <div class="col-lg-3 col-md-12">
                <div class="ts-service-wrapper brand">
                    <span class="service-img">
                        <img class="img-fluid" src="images/uno-minda.png" alt="">
                    </span> <!-- Service Img end -->                
                </div> <!-- Service wrapper end -->
            </div> <!-- Col end -->                       
        </div><!-- Row end -->
        <div class="gap-30"></div>


    </div><!-- Container end -->
</section><!-- Main container end -->


<?php include 'footer.php'; ?> 