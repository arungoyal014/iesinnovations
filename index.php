<?php include 'header.php'; ?>

<div class="ts-slider-area owl-carousel" style="background: url(images/slider-new.jpg) center no-repeat; background-size: cover;">
    <div class="slider-items slider-overlay">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-12">
                    <div class="slider-content">
                        <h1>Leading Manufacturing, Importing, Exporting and Trading Company</h1>                 

                    </div> <!-- Slider Content End -->
                </div> <!-- Col End -->
            </div> <!-- Row ENd -->
        </div> <!-- Container End -->
    </div> <!-- 1st Slider -->
    <div class="slider-items slider-overlay">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-12">
                    <div class="slider-content">
                        <h1>Expert Application Engineers</h1>                    
                    </div> <!-- Slider Content End -->
                </div> <!-- Col End -->
            </div> <!-- Row ENd -->
        </div> <!-- Container End -->
    </div> <!-- 1st Slider -->
    <div class="slider-items slider-overlay">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-12">
                    <div class="slider-content">
                        <h1>Skilled Service Team</h1>                     
                    </div> <!-- Slider Content End -->
                </div> <!-- Col End -->
            </div> <!-- Row ENd -->
        </div> <!-- Container End -->
    </div> <!-- 1st Slider -->

</div> <!-- Slider Area End -->


<section id="ts-service-bg" class="ts-service-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">
                    <span>Our</span> Solutions
                </h2>
            </div> <!-- Col End -->
        </div> <!-- Row End -->
    </div><!-- Container end -->
</section>

<section id="ts-service" class="ts-service pb-0">
    <div class="container">
        <div class="row">
            <div class="service-carousel owl-carousel">
                <div class="col-md-12">
                    <a href="pdfs/katashi-pneumatic-tools-2021.pdf" target="blank">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/3.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="pdfs/katashi-pneumatic-tools-2021.pdf" target="blank">PNEUMATIC TOOLS</a></h3>
                                <a href="pdfs/katashi-pneumatic-tools-2021.pdf" class="readmore" target="blank">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="pdfs/panasonic_power_tool_catalogu_asia.pdf" target="blank">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/4.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="pdfs/panasonic_power_tool_catalogu_asia.pdf" target="blank">BATTERY TOOLS</a></h3>
                                <a href="pdfs/panasonic_power_tool_catalogu_asia.pdf" class="readmore" target="blank">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="#" data-toggle="modal" data-target="#myModal3">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/1.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="#" data-toggle="modal" data-target="#myModal3">TORQUE WRENCHES</a></h3>
                                <a href="#" class="readmore" data-toggle="modal" data-target="#myModal3">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="javascript:void()" data-toggle="modal" data-target="#myModal4">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/10.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void()" data-toggle="modal" data-target="#myModal4">GAUGES</a></h3>
                                <a href="#" data-toggle="modal" data-target="#myModal4" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="javascript:void()" data-toggle="modal" data-target="#myModal5">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/error-profing.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void()" data-toggle="modal" data-target="#myModal5">ERROR PROOFING SYSTEMS</a></h3>
                                <a href="#" data-toggle="modal" data-target="#myModal5" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="javascript:void();" data-toggle="modal" data-target="#myModal6">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/8.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void();" data-toggle="modal" data-target="#myModal6">MEASURING INSTRUMENTS</a></h3>
                                <a href="#" data-toggle="modal" data-target="#myModal6" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="pdfs/volkel-catalog.pdf" target="blank">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/7.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="pdfs/volkel-catalog.pdf" target="blank">COILS</a></h3>
                                <a href="pdfs/volkel-catalog.pdf" target="blank" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>                       
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="pdfs/tools-equipment.pdf" target="blank">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/6.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="pdfs/tools-equipment.pdf" target="blank">HAND TOOLS</a></h3>
                                <a href="pdfs/tools-equipment.pdf" target="blank" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a> 
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                
                <div class="col-md-12">
                    <a href="#" data-toggle="modal" data-target="#myModal2">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/5.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="#" data-toggle="modal" data-target="#myModal2">SURFACE PLATES</a></h3>
                                <a href="#" data-toggle="modal" data-target="#myModal2" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="javascript:void();">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/9.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void();">PROFILE PROJECTOR</a></h3>
                                <br/>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="pdfs/Molygraph_General_Engineering_Brochure -2018.pdf" target="blank">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/12.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="pdfs/Molygraph_General_Engineering_Brochure -2018.pdf" target="blank">SPECIALITY LUBRICANT</a></h3>
                                <a href="pdfs/Molygraph_General_Engineering_Brochure -2018.pdf" target="blank" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="javascript:void()" data-toggle="modal" data-target="#myModal7">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/pidilite-image.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void()" data-toggle="modal" data-target="#myModal7">Lubricants, Adhesives and Sealants</a></h3>
                                <a href="javascript:void()" class="readmore" data-toggle="modal" data-target="#myModal7">Get Details<i class="fa fa-angle-double-right"></i></a>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                    <a href="#" data-toggle="modal" data-target="#myModal8">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/dial-stands.jpg" alt="">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="#" data-toggle="modal" data-target="#myModal8">Dial Stands</a></h3>
                                <a href="#" data-toggle="modal" data-target="#myModal8" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                                <br/>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
		<div class="col-md-12">
                    <a href="javascript:void()">
                        <div class="ts-service-wrapper">
                            <span class="service-img">
                                <img class="img-fluid" src="images/katashi-v-block.jpg" alt="" style="min-height: 280px;">
                            </span> <!-- Service Img end -->
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="icon-performance"></i>
                                </div> <!-- Service icon end -->
                                <h3><a href="javascript:void()">V BLOCK</a></h3>
                                <br/>
                            </div> <!-- Service content end -->
                        </div> <!-- Service wrapper end -->
                    </a>
                </div> <!-- Col end -->
                <div class="col-md-12">
                <a href="javascript:void()" data-toggle="modal" data-target="#myModal9">
                    <div class="ts-service-wrapper">
                        <span class="service-img">
                            <img class="img-fluid" src="images/gauge-measure-temperature.jpg" alt="">
                        </span> <!-- Service Img end -->
                        <div class="service-content">
                            <div class="service-icon">
                                <i class="icon-performance"></i>
                            </div> <!-- Service icon end -->
                            <h3><a href="javascript:void()" data-toggle="modal" data-target="#myModal9">Miscellaneous Solutions</a></h3>
                            <a href="#" data-toggle="modal" data-target="#myModal9" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                        </div> <!-- Service content end -->
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end -->
            <div class="col-md-12">
                <a href="pdfs/smartwasher-catalogue.pdf" target="_blank">
                    <div class="ts-service-wrapper">
                        <span class="service-img">
                            <img class="img-fluid" src="images/smartwasher-th.jpg" alt="">
                        </span> <!-- Service Img end -->
                        <div class="service-content">
                            <div class="service-icon">
                                <i class="icon-performance"></i>
                            </div> <!-- Service icon end -->
                            <h3><a href="pdfs/smartwasher-catalogue.pdf" target="_blank">SmartWasher</a></h3>
                            <a href="pdfs/smartwasher-catalogue.pdf" target="_blank" class="readmore">Get Details<i class="fa fa-angle-double-right"></i></a>
                            <br/>
                        </div> <!-- Service content end -->
                    </div> <!-- Service wrapper end -->
                </a>
            </div> <!-- Col end --> 
            </div> <!-- Service Carousel -->
        </div> <!-- Row End -->
    </div> <!-- Container end -->
</section> <!-- Service end -->
<hr>
<section id="ts-feature" class="ts-feature">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">
                    <span>Why</span> Choose Us
                </h2>
            </div><!-- Col end -->
        </div><!-- Row End -->
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ts-feature-wrapper">
                            <div class="feature-single">
                                <span class="feature-icon">
                                    <i class="icon-mechanic"></i>
                                </span><!-- feature icon -->
                                <div class="feature-content">
                                    <h3><span>Solution </span> Providers</h3>                              
                                </div><!-- feature content end -->
                            </div><!-- feature single end -->
                        </div><!-- feature wrapper end -->
                    </div><!-- Col end -->
                    <div class="col-md-6">
                        <div class="ts-feature-wrapper">
                            <div class="feature-single">
                                <span class="feature-icon">
                                    <i class="icon-price"></i>
                                </span><!-- feature icon -->
                                <div class="feature-content">
                                    <h3><span>Prompt  </span> After Sales Service</h3>                              
                                </div><!-- feature content end -->
                            </div><!-- feature single end -->
                        </div><!-- feature wrapper end -->
                    </div><!-- Col end -->
                </div><!-- Content Row End -->
                <div class="gap-35"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="ts-feature-wrapper">
                            <div class="feature-single">
                                <span class="feature-icon">
                                    <i class="icon-client"></i>
                                </span><!-- feature icon -->
                                <div class="feature-content">
                                    <h3><span>More </span> than 25 years of experience</h3>                              
                                </div><!-- feature content end -->
                            </div><!-- feature single end -->
                        </div><!-- feature wrapper end -->
                    </div><!-- Col end -->
                    <div class="col-md-6">
                        <div class="ts-feature-wrapper">
                            <div class="feature-single">
                                <span class="feature-icon">
                                    <i class="icon-fast"></i>
                                </span><!-- feature icon -->
                                <div class="feature-content">
                                    <h3><span>Sales </span> & Service network throughout India</h3>                              
                                </div><!-- feature content end -->
                            </div><!-- feature single end -->
                        </div><!-- feature wrapper end -->
                    </div><!-- Col end -->
                </div><!-- Content Row End -->
            </div><!-- Col End -->
        </div><!-- Row End -->
    </div><!-- Container end -->
    <div class="feature-img">
        <img class="img-fluid" src="images/why_choose_us_img.png" alt="">
    </div><!-- feature Img -->
</section><!-- Ts feature end -->

<section id="ts-pertner" class="ts-pertner solid-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="partner-carousel owl-carousel">
                    <figure class="partner-item partner-logo">
                        <a href="pdfs/katashi_catalogue_aug18.pdf" target="blank" ><img class="img-fluid" src="images/logo1.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="pdfs/panasonic_power_tool_catalogu_asia.pdf" target="blank" ><img class="img-fluid" src="images/logo2.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo3.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo4.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#" data-toggle="modal" data-target="#myModal" ><img class="img-fluid" src="images/logo5.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="pdfs/Kanon_catalog_ new.pdf" target="blank" ><img class="img-fluid" src="images/kanon.png" alt="Kanon Logo"></a>
                    </figure>
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo6.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo7.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="javascript:void()"><img class="img-fluid" src="images/logo8.png" alt=""></a>
                    </figure> <!-- Figure end -->                    
                    <figure class="partner-item partner-logo">
                        <a href="#" data-toggle="modal" data-target="#myModal2"><img class="img-fluid" src="images/logo10.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo11.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo12.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="pdfs/Molygraph_General_Engineering_Brochure -2018.pdf" target="blank" ><img class="img-fluid" src="images/logo13.png" alt=""></a>
                    </figure> <!-- Figure end -->                    
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/logo15.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/insize-logo.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/charnock-logo.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/pidilite-logo.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="#"><img class="img-fluid" src="images/meera-metzer-logo.png" alt=""></a>
                    </figure> <!-- Figure end -->
                    <figure class="partner-item partner-logo">
                        <a href="javascript:void()"><img class="img-fluid" src="images/logo9.png" alt=""></a>
                    </figure> <!-- Figure end -->

                </div> <!-- Partner carousel end -->
            </div> <!-- Col end -->
        </div> <!-- Row end -->
    </div> <!-- Container end -->
</section> <!-- Partner end -->

<?php include 'footer.php'; ?> 
