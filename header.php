<!DOCTYPE html>
<html>
    <head>   
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel='icon' href='images/favicon.png' type='image/x-icon'> 
        <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon'>

        <title>Industrial Engineering Solution</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/animate.css">

        <!-- IcoFonts -->
        <link rel="stylesheet" href="css/icofonts.css">
        <link rel="stylesheet" href="css/automobil_icon.css">

        <!-- Contact form -->   
        <link rel="stylesheet" href="css/contactme/contactme-1.3.css">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Owl Carousel -->
        <link rel="stylesheet" href="css/owlcarousel.min.css"> 

        <!-- Style -->
        <link rel="stylesheet" href="css/style.css?ver=1.5">
        <link href="css/jquery.treeview.css" type="text/css" rel="stylesheet" media="screen" />

        <!-- Responsive -->
        <link rel="stylesheet" href="css/responsive.css">
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176576938-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-176576938-2');
        </script>

    </head>

    <body>
        <div class="ts-top-bar">
            <div class="top-bar-angle">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-7"></div>
                        <div class="col-lg-4 col-md-5 text-right">
                            <div class="top-bar-event ts-top">
                                <i class="icon icon-clock"></i><span>We're Open: Mon - Sat 9:30 - 6:30</span>
                            </div> <!-- Top Bar Text End -->
                        </div> <!-- Col End -->               
                    </div> <!-- Row End -->
                </div> <!-- Container End -->
            </div> <!-- Angle Bar End -->
        </div> <!-- Top Bar End -->

        <header class="ts-header header-default">
            <div class="ts-logo-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-4 col-sm-4">
                            <a class="ts-logo" href="index.php" class="ts-logo">
                                <img src="images/ies-logo.png" alt="logo">
                            </a>
                        </div> <!-- Col End -->
                        <div class="col-md-8 col-sm-8 float-right">
                            <div class="mobilemenubg">
                                <div class="mobile-menu">
                                    <a href="javascript:void();">
                                        <img class="btnmenu" src="images/but_menu.png" alt="menu" />								
                                    </a>
                                </div>
                            </div>
                            <div class="mobileback"></div>
                            <div id="mobilemenu">					
                                <div class="mobilemenuclose">
                                    <img src="images/close.png" alt="close menu" border="0">
                                </div>
                                <div class="site-search">
                                    <form method="get" id="searchform-header" class="searchform-header" action="#">
                                        <input class="searchfield" name="s" type="text" placeholder="Search...">
                                        <button type="submit" class="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <div id="anpstext-8" class="widget widget_anpstext">        
                                    <ul class="contact-info">
                                        <li class="contact-info-item">
                                            <i class="fa fa-phone"></i><span class="important">Call us</span><br>
                                            <a href="tel:+91-78959-63836">+91-78959-63836</a> <br/> 
                                            <a href="tel:+91-97612-13836">+91-97612-13836</a> <br/> 
                                            <a href="tel:+91-84779-83000">+91-84779-83000</a>
                                        </li>
                                        <li class="contact-info-item">
                                            <i class="fa fa-envelope"></i><span class="important">Send us mail</span><br>
                                            <a href="mailto:dhananjay@iesinnovations.com">dhananjay@iesinnovations.com</a> <br/> 
                                            <a href="mailto:jagjeet@iesinnovations.com">jagjeet@iesinnovations.com</a>
                                        </li>
                                        <li class="contact-info-item">
                                            <i class="fa fa-external-link"></i><span class="important">We're Open</span><br>Mon - Sat 9:30 - 18:30
                                        </li>						

                                    </ul>
                                </div>                               						
                                <div class="mobilemenucontent">
                                    <ul id="browser">
                                        <li class="mobilemenuulli"><a href="index.php"><span>Home</span></a></li>
                                        <li class="mobilemenuulli"><a href="about.php"><span>About us</span></a></li>
                                        <li class="mobilemenuulli"><a href="product.php"><span>Our Solutions</span></a></li>
                                        <li class="mobilemenuulli"><a href="brands.php"><span>Our Brands</span></a></li>
                                        <li class="mobilemenuulli"><a href="clients.php"><span>Our Clients</span></a></li>
                                        <li class="mobilemenuulli"><a href="contact.php"><span>Contact Us</span></a></li>					  									</ul>
                                </div>
                            </div><!-- End of mobile menu -->
                            <ul class="top-contact-info">
                                <li>
                                    <span><i class="fa fa-phone"></i></span>
                                    <div class="info-wrapper">
                                        <p class="info-title">Call us</p>
                                        <p class="info-subtitle">
                                            <a href="tel:+91-78959-63836">+91-78959-63836</a> 
                                            <a href="tel:+91-97612-13836">+91-97612-13836</a> 
                                            <a href="tel:+91-84779-83000">+91-84779-83000</a>                                        
                                        </p>
                                    </div>
                                </li> <!-- li End -->
                                <li>
                                    <span><i class="fa fa-envelope"></i></span>
                                    <div class="info-wrapper" style="margin-right:0;">
                                        <p class="info-title">Send us mail</p>
                                        <p class="info-subtitle">
                                            <a href="mailto:dhananjay@iesinnovations.com">dhananjay@iesinnovations.com</a> 
                                            <a href="mailto:jagjeet@iesinnovations.com">jagjeet@iesinnovations.com</a>
                                        </p>
                                    </div>
                                </li> <!-- Li End -->	 

                            </ul> <!-- Contact info End -->
                        </div> <!-- Col End -->
                    </div> <!-- Row End -->
                </div> <!-- Container End -->
            </div> <!-- Logo End -->

            <div class="header-angle">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" aria-controls="navbarSupportedContent">
                            <div class="mobilemenubg">
                                <div class="mobile-menu">
                                    <a href="#">
                                        <img class="btnmenu" src="images/but_menu.png" alt="menu" />								
                                    </a>
                                </div>
                            </div>
                        </button><!-- End of Navbar toggler -->
                        <div class="collapse navbar-collapse justify-content-end ts-navbar" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="about.php"> About us</a></li>
                                <li class="nav-item"><a class="nav-link" href="product.php"> Our Solutions</a></li>
                                <li class="nav-item"><a class="nav-link" href="brands.php"> Our Brands</a></li>
                                <li class="nav-item"><a class="nav-link" href="clients.php"> Our Clients</a></li>
                                <li class="nav-item"><a class="nav-link" href="contact.php"> Contact Us</a></li>                    

                            </ul> <!-- End Navbar Nav -->
                        </div> <!-- End of navbar collapse -->
                    </nav> <!-- End of Nav -->
                </div> <!-- End of Container -->
            </div> <!-- End of Header Angle-->

        </header> <!-- End of Header area-->